# Getting Started #

git clone https://bitbucket.org/deaytan/plasmid-host-prediction/src/master/

# Prediction of plasmid hosts using random forests #

Plasmids are extra-chromosomal DNA sequences that can be hosted by both prokaryotes and eukaryotes. Plasmids might have broad and narrow host range. 
That means that broad host range plasmids might self-transfer, replicate and maintain in many organisms in contrast to the narrow host range organisms.
In this project (Aytan-Aktug et al,. 2021), the plasmid hosts were predicted using random forests. 

## Usage of PlasmidHostFinder

PlasmidHostFinder is a machine learning based tool that detects the host range of plasmids. Therefore, the user should know in advance 
whether the input sequences derived from plasmids or not (There are also tools available for identification of plasmids). Otherwise, PlasmidHostFinder accepts anything.
PlasmidHostFinder parses the input plasmid sequences into k-mers then runs the saved random forest models with the input k-mer matrix. The host range of the plasmids 
could be reported at species, genus, family and order levels. 

* If you have COMPLETE plasmid sequences, please use "plasmid_host_range_pipeline.py". 

* If you have PARTIAL plasmid sequences, please use "plasmid_host_range_pipeline_for_fragments.py".

### Saved machine learning models for both of the pipelines are avilable here:

    wget ftp://ftp.cbs.dtu.dk/public/CGE/databases/PlasmidHostFinder/*.pkl

In total, these models require 3.6 Gb space.  

If you are interested in specific models such as family level models for fragments, you can download only these models as shown below: 

    wget ftp://ftp.cbs.dtu.dk/public/CGE/databases/PlasmidHostFinder/*frag*family*.pkl

# plasmid_host_range_pipeline.py #

This pipeline uses the saved models to predict host of single or multiple plasmids. The pipeline requires a list for the input plasmids and the path for the actual dataset. This input 
list should include the input plasmid IDs with the file extensions such as .fna or .fasta. The pipeline accepts closed-circle plasmid data in fasta format. The pipeline calls KMC program to 
count the 8-mers. After that it generas a matrix and calls run_saved_model.py to predict the plasmid host for a given taxonomic level. The pipeline also asks for the 
probability class threshold to decide the classes. For the most cases, 0.5 is a valid probability class threshold. The saved models for the species level can be found 
in the required_input_data, if the user has not generated one, together with the other required files. This program generates two output files. Per plasmid, results.txt include the final 
prediction which the host probability is equal or greater than the class threshold, while probabilities.txt includes the all probabilities per host.


usage: plasmid_host_range_pipeline.py [-h] [-inp INPATH] [-i INPUTLIST] [-l LEVEL] [-t THRESHOLD] [-p PATH] [-o OUTDIR]


optional arguments:


  -h, --help:            show this help message and exit
  
  
  -inp INPATH, --inpath INPATH:  The path for the plasmid directory.
  
  
  -i INPUTLIST, --inputlist INPUTLIST:  Input list including the input plasmids
  
  
  -l LEVEL, --level LEVEL:  Taxonomy level
  
  
  -t THRESHOLD, --threshold THRESHOLD:  Class probability threshold
  
  
  -p PATH, --path PATH:  Path to the machine learning models
  
  
  -o OUTDIR, --outdir OUTDIR: Path to the output directory


# plasmid_host_range_pipeline_for_fragments.py #

This pipeline uses the saved fragment models for 500 bp, 1000 bp or 1500 bp to predict host of a single or multiple plasmids. The pipeline requires a list for the input plasmids and the path for the actual dataset. This input 
list should include the input plasmid IDs with the file extensions such as .fna or .fasta. The pipeline accepts closed-circle plasmid data in fasta format. Each plasmid sequence is sub-sampled into random region or regions
in size of given fragment size. If the given sequence is shorter than the given fragment size, the program will be terminated. After the fragmentation, the
KMC program is called to count the 8-mers. After that it generas a matrix and calls run_saved_model_fragments.py to predict the plasmid host for a given taxonomic level. The pipeline also asks for the 
probability class threshold to decide the classes. For the most cases, 0.5 is a valid probability class threshold. The saved models for the species level can be found 
in the required_input_data, if the user has not generated one, together with the other required files. Per plasmid, results.txt include the final prediction which the host probability is equal or greater than the class threshold, while probabilities.txt includes the all probabilities per host.

usage: plasmid_host_range_pipeline_for_fragments.py [-h] [-inp INPATH] [-i INPUTLIST] [-l LEVEL] [-t THRESHOLD] [-s FRAGSIZE] [-n SAMPLING] [-p PATH] [-o OUTDIR]


optional arguments:


  -h, --help:            show this help message and exit
  
  
  -inp INPATH, --inpath INPATH: The path for the plasmid directory.
  
  
  -i INPUTLIST, --inputlist INPUTLIST: Input list including the input plasmids
  
  
  -l LEVEL, --level LEVEL: Taxonomy level
  
  
  -t THRESHOLD, --threshold THRESHOLD: Class probability threshold
  
  
  -s FRAGSIZE, --fragsize FRAGSIZE: Fragment size; select either 500, 1000 or 1500
  
  
  -n SAMPLING, --sampling SAMPLING: Number of times each plasmid will be sampled
  
  
  -p PATH, --path PATH:  Path to the machine learning models
  
  
  -o OUTDIR, --outdir OUTDIR:  Path to the output directory
  
### Tips for running the pipelines 

1) The required input list should be the list of your inputs and will look like below:

    file1.fna
    file2.fna
    file3.fna
    ...

You can generate this file with FNA extensions using the example command below:

    (cd /path/to/your/input/directory/ && ls *.fna) > MyInputList.txt

2) If you need to install anaconda, the required anaconda2 version is 4.4.0. 


# Advance user: Train your host range predictor from scratch

If you are interested in generating your own machine learning models using your own plasmid data, please follow the steps below. 

# produce_id_feat_output.py #

This script is required for the preparing the input files for plasmid_host_prediction_RF.py. The program takes the path to the k-mer results, metadata for the plasmid hosts,
and the k-mer size. As output, it gives an k-mer count matrix, an output matrix for plasmid hosts in four taxonomy levels and corresponding features, also the ordered sample IDs. 

The program will only takes into consideration the plasmids, if the corresponding plasmid host observed in the input data at least five times. Also, it will remove the empty samples from the datasets. 

The higher level taxonomies were detected using Python ete2 package which uses NCBI Taxonomy. 

If the program fails to convert the species to higher level taxonomies, the failed taxonomy was named as "unrecognized". These samples with unrecognized taxonomies can be deleted using remove_col.py. 

This program detects the plasmid duplicates and remove from the dataset but takes into consideration the additional host information, if exists. 

This program calls prepare_multilabel_data.py to prepare outputs, matrix.py to build a k-mer count matrix, possible_duplicates.py to find and remove the plasmid duplicates and 
find_tax_ete2.py to convert the species to higher level taxonomies. 

usage: produce_id_feat_output.py [-h] [-p PATH] [-m META] [-k KMER]


optional arguments:


  -h, --help:    show this help message and exit
  
  
  -p PATH, --path PATH:  Path to the k-mer results
  
  
  -m META, --meta META:  Path to the metadata
  
  
  -k KMER, --kmer KMER:  K-mer size
  
  
# plasmid_host_prediction_RF.py #

This script predicts the plasmid hosts in different taxonomy levels (species, genus, family, order) from k-mer counts.
It requires an input matrix contains k-mer counts, corresponding outputs and ordered sample IDs. On the other hand, the desired taxonomy level, k-mer size, k-fold CV type should be provided. 
The program will train random forest models with a random subset of the input data. And the models will be assessed with the non-overlapping test and validation datasets. 
The program reports the model performances in AUC, MCC (Matthews correlation coefficient), F1 and confussion matrix. The MCC and F1 results will be reported for the thresholds from
0.1 to 0.9.

This script calls the stratified_cv.py and clusters.py programs for the stratified CV and clustered CV, respectively. 

usage: plasmid_host_prediction_RF.py [-h] [-k KMER] [-l LEVEL] [-m MATRIX] [-i IDS] [-t HOST] [-c CV] [-o OPT] [-r CLSTR] [-f FEAT]


optional arguments:


  -h, --help  show this help message and exit
  
  
  -k KMER, --kmer KMER:  K-mer size
  
  
  -l LEVEL, --level LEVEL:  Taxonomy level
  
  
  -m MATRIX, --matrix MATRIX:  Input matrix
  
  
  -i IDS, --ids IDS:  Input IDs
  
  
  -t HOST, --host HOST:  Host metadata
  
  
  -c CV, --cv CV:  K for k-fold CV
  
  
  -o OPT, --opt OPT:  Options for CV: 1 for stratified CV; 2 for clustered CV. IF you pick the number 2, a cluster file should be provided.
  
  
  -r CLSTR, --clstr CLSTR:  Cluster file for clustered CV
  
  
  -f FEAT, --feat FEAT:  Host names

# run_saved_model.py #

The plasmid_host_prediction_RF.py script will save the trained models into the working directory. The saved trained models will be numbered from one to five, one for each CV
model. The saved model names will also include the k-mer size and taxonomy level that the model trained with. The saved models are ready to use and it can be run with an independent 
dataset. The input file requirements are the same with plasmid_host_prediction_RF.py. 


usage: run_saved_model.py [-h] [-k KMER] [-l LEVEL] [-m MATRIX] [-s HOST] [-t THRESHOLD] [-p PATH] [-o OUTDIR]


optional arguments:


  -h, --help  show this help message and exit
  
  
  -k KMER, --kmer KMER:  K-mer size
  
  
  -l LEVEL, --level LEVEL:  Taxonomy level
  
  
  -m MATRIX, --matrix MATRIX:  Input matrix
  
  
  -s HOST, --host HOST:  Host metadata
  
  
  -t THRESHOLD, --threshold THRESHOLD:  Class probability threshold
  
  
  -p PATH, --path PATH:  Path to the machine learning models
  
  
  -o OUTDIR, --outdir OUTDIR: Path to the output directory

# run_fragments.py #

This program sub-samples the plasmid sequences for a given fragment size and for a given number of times. The program generates fragment fasta files and counts the k-mers for a given k.

usage: run_fragments.py [-h] [-p PATH] [-s FRAGSIZE] [-n NUMBER] [-k KMER]

optional arguments:

  -h, --help:            show this help message and exit
  
  -p PATH, --path PATH:  path to the list that includes plasmid names
  
  -s FRAGSIZE, --fragsize FRAGSIZE: Fragment size that will subsampled from the plasmid sequences
  
  -n NUMBER, --number NUMBER: number of times the subsampling will be happened
  
  -k KMER, --kmer KMER:  k-mer size

# plasmid_fragments_RF_model.py #

This program takes the partial plasmid sequences and predicts the host range of the origin plasmids. The program requires a path to the k-mer counts of the partial 
sequences. Also requires an id list for the origin plasmids and true host ranges to calculate the model performances. The partial sequence size may vary and the number of
sampling per plasmid may vary as well. The model automatically generates a matrix from the k-mer counts and saves. It seperates the input sequences into training, testing and validation datasets. The program makes sure that the partial sequences that origin from the same plasmids kept in the same partitions. In other words, two
partial sequences that origin from the same plasmid would be used either for training or testing or validation. The program reports the model performances in AUC, MCC, F1 and confussion matrix. The MCC and F1 results will be reported for the threshold of 0.5. 

usage: plasmid_fragments_RF_model.py [-h] [-p PATH] [-l LEVEL] [-k KMER] [-s FRAGSIZE] [-d IDS] [-t HOST] [-c CV] [-f FEAT]

optional arguments:
  -h, --help:            show this help message and exit
  
  -p PATH, --path PATH:  path to the fragments
  
  -l LEVEL, --level LEVEL: Taxonomy level
  
  -k KMER, --kmer KMER:  k-mer size
  
  -s FRAGSIZE, --fragsize FRAGSIZE: fragment size
  
  -d IDS, --ids IDS:     Input IDs
  
  -t HOST, --host HOST:  Host metadata
  
  -c CV, --cv CV:        K for k-fold CV
  
  -f FEAT, --feat FEAT:  Host names
  
# run_saved_model_fragments.py #

The plasmid_fragments_RF_model.py script will save the trained models into the working directory. The saved trained models will be numbered from one to five, one for each CV
model. The saved model names will also include the k-mer size, fragment  size and taxonomy level that the model trained with. The saved models are ready to use and it can be run with an independent 
dataset. The input file requirements are the same with plasmid_fragments_RF_model.py. 

usage: run_saved_model_fragments.py [-h] [-k KMER] [-l LEVEL] [-m MATRIX] [-s HOST] [-t  THRESHOLD] [-f FRAGSIZE] [-p PATH] [-o OUTDIR]

optional arguments:

  -h, --help:            show this help message and exit
  
  -k KMER, --kmer KMER:  K-mer size
  
  -l LEVEL, --level LEVEL:  Taxonomy level
  
  -m MATRIX, --matrix MATRIX:  Input matrix
  
  -s HOST, --host HOST:  Host metadata
  
  -t THRESHOLD, --threshold THRESHOLD:  Class probability threshold
  
  -f FRAGSIZE, --fragsize FRAGSIZE:  Fragment size for the right model selection  
  
  -p PATH, --path PATH:  Path to the machine learning models
  
  -o OUTDIR, --outdir OUTDIR: Path to the output directory

# Example program usage #

##Prepare the input matrices and outputs 

python produce_id_feat_output.py -p ./toy_data/ -m ./toy_data/PATRIC_plasmids_all.txt -k 5

##Run the random forest model from scratch

python plasmid_host_prediction_RF.py -k 5 -l order -m plasmid_5_mat.npy -i plasmid_5_ids.npy  -t plasmid_5_outs_order.npy -c 3 -o 1 -f plasmid_5_features_order.npy

# Pre-required python packages and programs #

## Python2 libraries ##

* numpy
* ete2
* sklearn
* argparse
* collections
* itertools
* math
* os
* pickle
* random
* sys

## Programs ##

* KMC (version: 3.0.0) 

KMC3 can be downloaded from here: http://sun.aei.polsl.pl/REFRESH/index.php?page=projects&project=kmc&subpage=download

Please make sure that KMC is copied into your bin directory. 

## Downloads ##

The supplementary files (Aytan_Aktug_PlasmidHostFinder_Supp_Tables and Aytan_Aktug_PlasmidHostFinder_Supp_Figures) for the PlasmidHostFinder paper (https://www.biorxiv.org/content/10.1101/2021.09.27.462084v1.abstract) are available in downloads. 



