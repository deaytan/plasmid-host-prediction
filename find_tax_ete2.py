import numpy as np
from ete2 import NCBITaxa
import collections 

def tax_levels(spe, spe_out, level):

	ncbi = NCBITaxa()

	new_spe = []

	new_temp = []

	for i in spe:

		#i = str(i[1])

		i = i.replace("Cyanothece sp. PCC 7424", "Gloeothece citriformis")
		i = i.replace("Aster yellows", "Candidatus Phytoplasma asteris")
		i = i.replace("Onion yellows", "Candidatus Phytoplasma asteris")	
		i = i.replace("Paraburkholderia sp. BN5", "Paraburkholderia aromaticivorans")
		i = i.replace("Periwinkle leaf", "Candidatus Phytoplasma asteris")
		i = i.replace("Phyllobacterium sp. Tri-48", "Phyllobacterium zundukense")
		i = i.replace("Sphingobium sp. C1", "Sphingobium hydrophobicum")	
		i = i.replace("Borrelia garinii", "Borreliella garinii")
		i = i.replace("Borrelia burgdorferi", "Borreliella burgdorferi")

		new_spe.append(i)

		name2taxid = ncbi.get_name_translator([i]) 
		line = ncbi.get_lineage(name2taxid[i][0])


		status = 0

		for l in line:
			tax  = ncbi.get_rank([l])

			if tax.values()[0].decode("ascii") == level:
				order_id = tax.keys()
				order_corr = ncbi.get_taxid_translator([order_id][0]).values()[0].decode("ascii")
			
				new_temp.append(str(order_corr))

				status = 1
		if status == 0:
			print("could not find", i)
			new_temp.append("unrecognized")

	if level == "species":
		new_temp = new_spe


	uniq_new_temp = list(set(new_temp))

	mat = np.zeros((len(spe_out), len(uniq_new_temp)))

	for i in range(len(spe_out)):
		hosts = [n for n,x in enumerate(spe_out[i]) if x == 1]

		for h in hosts:
			ind = uniq_new_temp.index(new_temp[h])
			mat[i, ind] = 1
	
	##save the results##

	return(mat, uniq_new_temp)








