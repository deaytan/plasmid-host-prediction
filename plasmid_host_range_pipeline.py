import os
import numpy as np
import argparse
import sys

if(__name__ == "__main__"):

	##Ask for the inputs

	parser = argparse.ArgumentParser()

	parser.add_argument("-inp", "--inpath", required = True, help = "The path for the plasmid directory.")
	parser.add_argument("-i", "--inputlist", required = True, help = "Input list including the input plasmids")
	parser.add_argument("-l", "--level", required= True, help = "Taxonomy level")
	parser.add_argument("-t", "--threshold", required= True, help = "Class probability threshold") 
	parser.add_argument("-p", "--path", required = True, help = "Path to the machine learning models")
	parser.add_argument("-o", "--outdir", required=True, help = "Path to the output directory")

	args = parser.parse_args()

	inp = args.inpath
	inlist = np.loadtxt(args.inputlist, dtype = "string")
	level = args.level
	thr = float(args.threshold)
	path = args.path
	outdir = args.outdir

	rd, fn = os.path.split(os.path.abspath(__file__))

	k = 5

	##make an output directory

	os.system("mkdir %s" % outdir)

	##Run KMC

	kmers = []

	if inlist.ndim == 1:
		pass
	
	else:
		inlist = [inlist]

	for each in inlist:

		name = str(each).replace(".fasta", "").replace(".fna", "")

		os.system("kmc -t1 -k8 -fm -ci1 -cs1677215 %s/%s %s/%s.res %s/out" % (inp, each, outdir, name, outdir))
		os.system("kmc_dump %s/%s.res %s/%s.kms" % (outdir, name, outdir, name))

		kmers.append("%s.kms" %  name)

		##clean up
		os.system("rm -r %s/%s.res.kmc_pre" % (outdir, name))
		os.system("rm -r %s/%s.res.kmc_suf" % (outdir, name))


	##Make sure that the saved model and the new matrix has the same matrix order
	order = np.loadtxt("%s/required_input_data/order_of_8mers.txt" % rd, dtype = "string", delimiter = "\t")

	##Generate the input matrix
	mat = np.zeros((len(kmers), len(order)))
	
	samples =  []

	for e in range(len(kmers)):
		
		each = kmers[e]

		if each in os.listdir(outdir):

			counts = np.loadtxt("%s/%s" % (outdir, each), dtype = "string", delimiter = "\t")

			if len(counts) != 0:
				
				samples.append(each.replace(".kms", ""))

				for i in range(len(order)):

					if order[i] in list(counts[:,0]):
						ind = list(counts[:,0]).index(order[i])

						mat[e,i] = int(counts[ind,1])

	##delete empty rows
	empty = []
	for row in range(len(mat)):
		if sum(mat[row]) == 0:
			empty.append(row)
			
	mat = np.delete(mat, empty, 0)

	##save the matrix
	np.save("%s/mat" % outdir, mat)
	np.save("%s/order_of_samples" % outdir, samples)

	##Run the saved models
	os.system("python %s/run_saved_model.py -k 8 -l %s -m %s/mat.npy -t %s -s %s/required_input_data/plasmid_model_8mer_features_%s_ete2.npy -o %s -p %s" % (rd, level, outdir, thr, rd, level, outdir, path))

	##cleanup
	os.system("rm -r %s/mat.npy" % outdir)

	##Show the results
	os.system("cat %s/results.txt" % outdir)
