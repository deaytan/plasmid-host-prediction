from prepare_multilabel_data import prepare, one_hot, multiclass
import numpy as np
import matrix
import argparse
import os
from possible_duplicates import no_duplicates
from find_tax_ete2 import tax_levels

if(__name__ == "__main__"):

	parser = argparse.ArgumentParser()

	parser.add_argument("-p", "--path", required= True, help = "Path to the k-mer results")
	parser.add_argument("-m", "--meta", required= True, help = "Path to the metadata")
	parser.add_argument("-k", "--kmer", required= True, help = "K-mer size")

	args = parser.parse_args()

	path = args.path
	meta = args.meta
	kmer = args.kmer

	##Prepare required files##

	os.system("cat %s/*.kms | cut -f1 | sort -u > %s/possible_mers.txt" % (path, path))
	os.system("ls %s/*.kms | rev | cut -d/ -f1 | rev> %s/plasmids.txt" % (path, path))

	##Prepare outputs##

	res = prepare("%s" % meta) ##read the metadata and prepare the outputs

	out = one_hot(res)

	out_ids = out[0] ##ids
	out_feat = out[1] ##species features
	out_put = out[2] ##outputs

	##Prepare outputs in multilabel##

	out2 = multiclass(out)

	out2_ids = out2[0]
	out2_feat = out2[1]
	out2_put = out2[2]

	##convert features to list##

	out2_feat_list = []

	feats = str(out2_feat).split(",")

	for i in feats[1:]:
		#key = i.split(":")[0].replace(" ", "").replace("{", "").replace("}", "")
		value = i.split(":")[1].replace("[", "").replace("]", "").replace("'", "").replace("{", "").replace("}", "").replace("(", "").replace(")", "")[1:]
		out2_feat_list.append(value)

	##Prepare inputs##

	##call the matrix function

	mat = matrix.prep_matrix("possible_mers.txt", "plasmids.txt", path, "matrix_%s" % kmer) ##ordered input data

	##Input order##

	order = np.loadtxt("%s/plasmids.txt" % path, dtype = "string")

	##Delete the mat rows, if does not have an appropriate metadata##

	ordered = []
	mat_order = []
	for i in range(len(order)):
		o = order[i].split(".%s.kms" % kmer.replace("mer", ""))[0]
		if o in out2_ids:
			ind = list(out2_ids).index(o)
			ordered.append(ind)
		else:
			mat_order.append(i)

	out2_ids = out2_ids[ordered] ##ordered ids
	out2_put = out2_put[ordered] ##ordered output
	mat = np.delete(mat, mat_order, axis  = 0) ##ordered mat

	##find the empty lines##

	deleted = []
	for i in range(len(mat)):
		if sum(mat[i])== 0:
			deleted.append(i)		

	new_mat = np.delete(mat, deleted, axis = 0)
	new_out = np.delete(out2_put, deleted, axis = 0)
	new_ids = np.delete(out2_ids, deleted, axis = 0)

	##find the duplicates## 

	edited_mat, edited_ids, edited_out = no_duplicates(path, kmer, new_mat, new_ids, new_out)

	##find the outputs in different taxonomy levels##

	levels = ["species", "genus", "family", "order"]

	for level in levels:
		mod_out, mod_feat = tax_levels(out2_feat_list, edited_out, level)

		np.save("plasmid_%s_features_%s" % (kmer, level), mod_feat)
		np.save("plasmid_%s_outs_%s" % (kmer, level), mod_out)

	np.save("plasmid_%s_mat" % kmer, edited_mat)
	np.save("plasmid_%s_ids" % kmer, edited_ids)




