###This program aims to predict plasmid hosts using random forest model
##The required inputs are k-mer matrix, host metadata, prediction taxonomy level

import numpy as np
from sklearn.metrics import roc_curve, auc, accuracy_score, f1_score, matthews_corrcoef, roc_auc_score, confusion_matrix
from sklearn.ensemble import RandomForestClassifier
from sklearn.multiclass import OneVsRestClassifier
from stratified_cv import stratified
import itertools
import clusters
import random
import collections
import sys
import pickle
import argparse 

if(__name__ == "__main__"):

	parser = argparse.ArgumentParser()

	parser.add_argument("-k", "--kmer", required= True, type = int, help = "K-mer size")
	parser.add_argument("-l", "--level", required= True, help = "Taxonomy level")
	parser.add_argument("-m", "--matrix", required= True, help = "Input matrix")
	parser.add_argument("-i", "--ids", required= True, help = "Input IDs")
	parser.add_argument("-t", "--host", required= True, help = "Host metadata")
	parser.add_argument("-c", "--cv", required= True, type = int, help = "K for k-fold CV")
	parser.add_argument("-o", "--opt", required= True, type = int, help = "Options for CV: 1 for stratified CV; 2 for clustered CV. IF you pick the number 2, a cluster file should be provided.")
	parser.add_argument("-r", "--clstr", help = "Cluster file for clustered CV")
	parser.add_argument("-f", "--feat", help = "Host names")

	args = parser.parse_args()

	###input arguments###

	kmersize = args.kmer
	level = args.level
	out2_put = np.load(args.host)
	out2_ids = np.load(args.ids)
	mat = np.load(args.matrix)
	feats = np.load(args.feat)

	k = args.cv

	###cross validation###

	if args.opt == 1:

		##stratified CV

		cv = stratified(out2_put, k)

		##pick random validation samples

		validation = []

		for i in range(len(cv)):

			val = random.sample(cv[i], len(cv[i])/6)

			for v in val:

				cv[i].remove(v)		
				validation.append(v)

	elif args.opt == 2:

		##clustered cv

		dictm = clusters.clusters(args.clstr)

		val = random.sample(dictm.keys(), len(dictm)/6)

		rest = list(set(dictm.keys()) - set(val))

		dictm_val = collections.defaultdict(list)

		dictm_cv = collections.defaultdict(list)

		for i in val:
			dictm_val[i] = dictm[i]

		for i in rest:
			dictm_cv[i] = dictm[i]

		##CV values

		cv_raw = clusters.clustered_cv(dictm_cv, k)

		cv = clusters.clustered_ind(cv_raw, out2_ids)

		##Validation values

		val_raw = clusters.clustered_cv(dictm_val,1)

		validation = clusters.clustered_ind(val_raw, out2_ids)[0]

	##remove the unrecognized classes by NCBI Taxonomy

	if "unrecognized" in feats: 

		d_cols, d_rows = detect_remove_cols.remove(out2_put, feats)

		for i in range(len(cv)):
			for d in d_rows:
				if d in cv[i]:
					cv[i].remove(d)

		for v in validation:
			if v in d_rows:
				validation.remove(v)

		out2_put = np.delete(out2_put, d_cols, axis = 1)

	###training & testing & validation###

	##number of ouputs

	n_class = len(out2_put[0])

	##save the results into one file for CV 

	mcc_temp = collections.defaultdict(list)

	f1_temp = collections.defaultdict(list)

	auc_temp = []

	tn_temp = []

	tp_temp = []

	fn_temp = []

	fp_temp = []

	##validation input data

	validation_x = mat[validation]

	validation_y = out2_put[validation]

	##save validation results

	all_val_pred = []

	auc_val = []

	mcc_val = collections.defaultdict(list)

	f1_val = collections.defaultdict(list)

	thresholds = [0.9, 0.8, 0.7, 0.6, 0.4, 0.3, 0.2, 0.1, 0.5]

	for i in range(k):

		test_index = cv[i]

		train_index = []

		for c in list(set(range(k)) - set([i])):
			train_index.append(cv[c])

		train_index = list(itertools.chain.from_iterable(train_index))

		train_data_x, test_data_x = mat[train_index], mat[test_index]
		train_data_y, test_data_y = out2_put[train_index], out2_put[test_index]

		clf_1 = OneVsRestClassifier(RandomForestClassifier(n_estimators = 2000, bootstrap = True, min_samples_leaf = 1, max_features = "sqrt", min_samples_split = 5, max_depth = 100, random_state = 42, class_weight = "balanced", n_jobs = 8))
		clf_1.fit(train_data_x, train_data_y)

		##save the model

		filename = "Model_%s_%s_%s.sav" % (kmersize, level, str(i))

		pickle.dump(clf_1, open(filename, "wb"))

		##prediction class and probabilities

		test_pred = clf_1.predict(test_data_x)
		test_prob = clf_1.predict_proba(test_data_x)

		train_pred = clf_1.predict(train_data_x)
		train_prob = clf_1.predict_proba(train_data_x)

		val_pred = clf_1.predict(validation_x)
		val_prob = clf_1.predict_proba(validation_x)

		all_val_pred.append(val_prob)
		
		merged_truth = []

		merged_prob = []

		for n in range(n_class):
			for element in test_data_y[:,n]:
				merged_truth.append(element)
			for item in test_prob[:,n]:
				merged_prob.append(item)

		fpr, tpr, threshold = roc_curve(merged_truth, merged_prob, pos_label = 1)

		##ROC AUC score

		if np.isnan(auc(fpr, tpr)) == True:
			auc_temp.append(np.nan)
			for t in thresholds:
				mcc_temp[t].append(np.nan)
				f1_temp[t].append(np.nan)
			tn_temp.append(np.nan)
			fp_temp.append(np.nan)
			fn_temp.append(np.nan)
			tp_temp.append(np.nan)	
			print("nan detected", n)
			continue

		auc_temp.append(auc(fpr, tpr))

		##MCC and F1

		for t in thresholds:

			a_pred_t = []

			for pt in merged_prob:
				if pt < t:
					a_pred_t.append(0)
				else:
					a_pred_t.append(1)

			mccm = matthews_corrcoef(merged_truth, a_pred_t)

			f1m = f1_score(merged_truth, a_pred_t, average = "macro")

			mcc_temp[t].append(mccm)

			f1_temp[t].append(f1m)

		##Confussion matrix

		tn, fp, fn, tp = confusion_matrix(merged_truth, a_pred_t).ravel()

		tn_temp.append(tn)

		fp_temp.append(fp)

		fn_temp.append(fn)

		tp_temp.append(tp)

	###validation results##

	##take an average of class probabilities##

	mean_val_pred = np.average(all_val_pred, axis = 0)

	merged_pred_val = []

	merged_truth_val = []

	for n in range(n_class):

		for element in mean_val_pred[:,n]:
			merged_pred_val.append(element)

		for item in validation_y[:,n]:
			merged_truth_val.append(item)		

	##AUC for validation

	fpr_v, tpr_v, threshold_v = roc_curve(merged_truth_val, merged_pred_val, pos_label = 1)

	auc_val.append(auc(fpr_v, tpr_v))

	##MCC and F1 for validation

	for t in thresholds:

		a_pred_v = []
		for p in merged_pred_val:
			if p < t:
				a_pred_v.append(0)
			else:
				a_pred_v.append(1)

		mcc_val[t].append(matthews_corrcoef(merged_truth_val, a_pred_v))
		f1_val[t].append(f1_score(merged_truth_val, a_pred_v, average = "macro"))

	##Confussion matrix for validation

	tn, fp, fn, tp = confusion_matrix(merged_truth_val, a_pred_v).ravel()


	##Report the performances##

	print("##Confussion matrix of CV##")

	print("tp", np.nanmean(tp_temp))
	print("tn", np.nanmean(tn_temp))
	print("fp", np.nanmean(fp_temp))
	print("fn", np.nanmean(fn_temp))


	print("##Confussion matrix for validation##")

	print("tp val", tp)
	print("tn val", tn)
	print("fp val", fp)
	print("fn val", fn)

	print("##AUC results for CV and validation##")

	print("AUC test", np.nanmean(auc_temp))
	print("AUC test std", np.nanstd(auc_temp))
	print("AUC val", np.nanmean(auc_val))

	print("##MCC and F1 results for CV and validation##")

	print("metric", "threshold", "data", "performance")

	for t in thresholds:

		print("mcc", t, "test", np.nanmean(mcc_temp[t]))
		print("mcc", t, "val", np.nanmean(mcc_val[t]))

		print("f1", t, "test", np.nanmean(f1_temp[t]))
		print("f1", t, "val", np.nanmean(f1_val[t]))


