import numpy as np
import collections
	
def prep_matrix(all_comb, inp_list, path, name):
	##This function aims to prepare a matrix

	kmers = np.loadtxt("%s/%s" % (path, all_comb), dtype = "string", delimiter = "\t")

	ins = np.loadtxt("%s/%s" % (path, inp_list), dtype = "string", delimiter = "\t")

	mat = np.zeros((len(ins), len(kmers)), dtype=int)
	
	kmers_dict = collections.defaultdict(list)

	##make a dict from all possible kmers

	for i in range(len(kmers)):
		kmers_dict[kmers[i]].append(i)
	
	for i in range(len(ins)):
		kms = np.loadtxt("%s/%s" % (path, ins[i]), dtype = "string", delimiter = "\t")
		
		for k in kms:
			mat[i, kmers_dict[k[0]]] = k[1]

	#np.save(name, mat)

	return mat

	

	
	

	

	




