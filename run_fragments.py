from subsample import subsample
import numpy as np
import sys
import argparse

if(__name__ == "__main__"):

	parser = argparse.ArgumentParser()

	parser.add_argument("-p", "--path", required= True, help = "path to the list that includes plasmid names")
	parser.add_argument("-s", "--fragsize", required = True, help = "Fragment size that will subsampled from the plasmid sequences")
	parser.add_argument("-n", "--number", required = True, type = int, help = "number of times the subsampling will be happened")
	parser.add_argument("-k", "--kmer", required = True, type = int, help = "k-mer size")

	###input arguments###

	args = parser.parse_args()

	isolates = np.loadtxt(args.path, dtype = "string")
	frag_size = args.fragsize
	n = args.number
	kmer = args.kmer

	for i in isolates: 
		subsample(i, frag_size, "./fragments_%imer_%i" % (kmer,frag_size), n, kmer)







