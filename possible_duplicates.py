import numpy as np
from find_possible_duplicates import find_duplicates


def no_duplicates(path, kmer, mat, ids, outs):

	identical = find_duplicates(path)

	pairs = []

	for i in identical:
		
		pair_1 = i[1].replace(".%s.kms" % kmer, "")
		pair_2 = i[2].replace(".%s.kms" % kmer, "")

		if pair_1 in ids and pair_2 in ids:

			#print("yes")
		
			ind_1 = list(ids).index(pair_1)
			ind_2 = list(ids).index(pair_2)

			out_1 = [n for n,x in enumerate(list(outs[ind_1])) if x == 1]
			out_2 = [n for n,x in enumerate(list(outs[ind_2])) if x == 1]


			if [pair_1, pair_2] not in pairs and [pair_2, pair_1] not in pairs:
				pairs.append([pair_1, pair_2, ind_1, ind_2, out_1, out_2])

	#######Fix the species data########

	deleted = [] ##save the indices that to be deleted

	for i in pairs:

		deleted.append(i[3])

		for alt in i[5]:

			outs[i[2], alt] = 1 

	for i in range(len(outs)):
		if 1 not in list(outs[i]):
			deleted.append(i)

	edited_mat = np.delete(mat, deleted, axis = 0)
	edited_ids = np.delete(ids, deleted, axis = 0)
	edited_outs = np.delete(outs, deleted, axis = 0)

	return edited_mat, edited_ids, edited_outs


