import random

import os
	
random.seed(42)

def subsample(inp, fragment_size, path, t, kmer):

	inp_open = open(inp, "r")

	inp_reads = inp_open.readlines()

	if ">" in inp_reads[0]:

		inp_reads = "".join(inp_reads[1:])

	else:
		inp_reads = "".join(inp_reads[0:])
	
	inp_open.close()

	if len(inp_reads) > fragment_size:

		for times in range(t):

			start  = random.randint(0, len(inp_reads) - fragment_size)

			fragment = inp_reads[start:start+fragment_size]

			outp = open("%s/%s" % (path, "temp.txt"), "w")

			outp.write(">tempfile\n")

			for i in fragment: 
				outp.write(i)

			outp.close()

			##calculate k-mers

			name = inp.split("/")[-1]

			name = name.replace(".fna", "")
			name = name.replace(".fasta", "")

			os.system("kmc -k%s -fm -ci1 -cs1677215 %s/temp.txt %s/%s_%i.res out" % (kmer, path, path, name, start))

			os.system("kmc_dump %s/%s_%i.res %s/%s_%i.%s.kms" % (path, name, start, path, name, start, kmer))








