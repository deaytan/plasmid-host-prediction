import numpy as np

np.random.seed(42)

def stratified(output, k):

	##this function splits data into k partitions by taking into consideration of output ratios in the population for multi-label output.	

	cv = [[] for x in xrange(k)] 

	used = []

	for i in range(len(output[0])):
	
		ratio =  int(sum(output[:,i])/k)


		indexes = [m for m,x in enumerate(list(output[:,i])) if x == 1]

		np.random.shuffle(indexes)

		a = 0
		b = a + ratio 
		for n in range(k):
			if n != k-1:
				for s in indexes[a:b]:
					if s not in used:
						cv[n].append(s)
						used.append(s)
		
			elif n == k-1:
				for s in indexes[a:]:
					if s not in used:
						cv[n].append(s)
						used.append(s)
			a = b
			b = a + ratio 

	return cv
	



