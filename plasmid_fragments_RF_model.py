import numpy as np
from sklearn.metrics import roc_curve, auc, matthews_corrcoef, confusion_matrix,f1_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.multiclass import OneVsRestClassifier
import itertools
import fragments
import random
import argparse
import os
import pickle

if(__name__ == "__main__"):

	parser = argparse.ArgumentParser()

	parser.add_argument("-p", "--path", required= True, help = "path to the fragments")
	parser.add_argument("-l", "--level", required= True, help = "Taxonomy level")
	parser.add_argument("-k", "--kmer", required = True, type = int, help = "k-mer size")
	parser.add_argument("-s", "--fragsize", required = True, help = "fragment size")
	parser.add_argument("-d", "--ids", required= True, help = "Input IDs")
	parser.add_argument("-t", "--host", required= True, help = "Host metadata")
	parser.add_argument("-c", "--cv", required= True, type = int, help = "K for k-fold CV")
	parser.add_argument("-f", "--feat", required = True, help = "Host names")

	#####################Data preparation part##################

	###input arguments###

	args = parser.parse_args()

	path = args.path
	level = args.level
	kmer = args.kmer
	frag_size = args.fragsize
	outp = np.load(args.host)
	ids = np.load(args.ids)
	k = args.cv
	feats = np.load(args.feat)


	##prepare the input files

	out = fragments.prep_matrix(path, ids) ##call the function to generate a matrix from fragments

	mat = out[0]

	used_frag = out[1]

	output, ids = fragments.matrix_fragment_output(used_frag, ids, outp) ## prepare the outputs and order of the fragments 

	#####################Machine learning model####################

	##cross validation with no clustering

	random.seed(42)

	cv = fragments.fragment_cv(ids, k)

	validation = []

	for i in range(len(cv)):
		val = random.sample(cv[i], len(cv[i])/6)

		for v in val:

			cv[i].remove(v)		
			validation.append(v)

	##remove the unrecognized classes by NCBI Taxonomy

	if "unrecognized" in feats: 

		d_cols, d_rows = detect_remove_cols.remove(output, feats)

		for i in range(len(cv)):
			for d in d_rows:
				if d in cv[i]:
					cv[i].remove(d)

		for v in validation:
			if v in d_rows:
				validation.remove(v)

		output = np.delete(output, d_cols, axis = 1)

	##validation dataset

	val_x = mat[validation]
	val_y = output[validation]

	##training & testing

	all_val_pred = []

	auc_temp = []

	mcc_temp = []

	f1_temp = []

	tn_temp = []

	tp_temp = []

	fn_temp = []

	fp_temp = []

	for i in range(k):

		test_index = cv[i]

		train_index = []

		for c in list(set(range(k)) - set([i])):
			train_index.append(cv[c])

		train_index = list(itertools.chain.from_iterable(train_index))

		train_data_x, test_data_x = mat[train_index], mat[test_index]
		train_data_y, test_data_y = output[train_index], output[test_index]

		clf_1 = OneVsRestClassifier(RandomForestClassifier(n_estimators = 1000, bootstrap = False, min_samples_leaf = 1, max_features = "auto", min_samples_split = 2, max_depth = 50, random_state = 42, class_weight = "balanced", n_jobs = 1))
		clf_1.fit(train_data_x, train_data_y)
		
		##save the model

		filename = "Model_fragment_%s_%s_%s_%s.sav" % (frag_size, kmer, level, str(i))

		pickle.dump(clf_1, open(filename, "wb"))

		test_pred = clf_1.predict(test_data_x)
		test_prob = clf_1.predict_proba(test_data_x)

		val_pred = clf_1.predict(val_x)
		val_prob = clf_1.predict_proba(val_x)

		all_val_pred.append(val_prob)

		n_class = len(test_data_y[0])

		##ROC-AUC

		merged_pred = []
		merged_truth = []

		for i in range(n_class):

			for element in test_data_y[:,i]:

				merged_truth.append(element)

			for item in test_prob[:,i]:

				merged_pred.append(item)

		fpr, tpr, thres = roc_curve(merged_truth, merged_pred, pos_label = 1)

		if np.isnan(auc(fpr, tpr)) == True:
			auc_temp.append(np.nan)
			mcc_temp.append(np.nan)	
			continue

		auc_temp.append(auc(fpr, tpr))

		##MCC & F1 & Confusion matrix

		pred_t = []

		for r in merged_pred:

			if r < 0.5:
				pred_t.append(0)

			else:
				pred_t.append(1)

		mcc_temp.append(matthews_corrcoef(merged_truth, pred_t))

		f1_temp.append(f1_score(merged_truth, pred_t, average = "macro"))

		tn, fp, fn, tp = confusion_matrix(merged_truth, pred_t).ravel()

		tn_temp.append(tn)

		fp_temp.append(fp)

		fn_temp.append(fn)

		tp_temp.append(tp)

	##validation score

	##ROC-AUC

	mean_val_pred = np.average(all_val_pred, axis = 0)

	merged_pred_val = []

	merged_truth_val = []

	for n in range(n_class):

		for element in val_y[:,n]:

			merged_truth_val.append(element)

		for item in mean_val_pred[:,n]:

			merged_pred_val.append(item)

	fpr_v, tpr_v, threshold_v = roc_curve(merged_truth_val, merged_pred_val, pos_label = 1)

	auc_val = auc(fpr_v, tpr_v)

	##MCC & F1 & Confusion matrix

	a_pred_v = []
	for n in range(n_class):
		for p in mean_val_pred[:,n]:
			if p < 0.5:
				a_pred_v.append(0)
			else:
				a_pred_v.append(1)

	mcc_val = matthews_corrcoef(merged_truth_val, a_pred_v)

	f1_val = f1_score(merged_truth_val, a_pred_v, average = "macro")

	tn_val, fp_val, fn_val, tp_val = confusion_matrix(merged_truth_val, a_pred_v).ravel()

	##Averaged results
	print("tp_test", np.nanmean(tp_temp))
	print("tn_test", np.nanmean(tn_temp))
	print("fp_test", np.nanmean(fp_temp))
	print("fn_test", np.nanmean(fn_temp))

	print("#####")
	print("tp_val", tp_val)
	print("tn_val", tn_val)
	print("fp_val", fp_val)
	print("fn_val", fn_val)

	print("#####")
	print("AUC test", np.nanmean(auc_temp))
	print("AUC test std", np.nanstd(auc_temp))
	print("AUC val", auc_val)

	print("#####")
	print("MCC test", np.nanmean(mcc_temp))
	print("MCC test std", np.nanstd(mcc_temp))
	print("MCC val", mcc_val)

	print("#####")
	print("F1 test", np.nanmean(f1_temp))
	print("F1 test std", np.nanstd(f1_temp))
	print("F1 val", f1_val)
