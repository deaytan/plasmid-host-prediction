import pickle
import numpy as np
import sys
import argparse 
import os
from sklearn.externals import joblib

if(__name__ == "__main__"):

	parser = argparse.ArgumentParser()

	parser.add_argument("-k", "--kmer", required= True, type = int, help = "K-mer size")
	parser.add_argument("-l", "--level", required= True, help = "Taxonomy level")
	parser.add_argument("-m", "--matrix", required= True, help = "Input matrix")
	parser.add_argument("-s", "--host", required= True, help = "Host metadata")
	parser.add_argument("-t", "--threshold", required= True, help = "Class probability threshold")
	parser.add_argument("-p", "--path", help = "Path to the machine learning models")
	parser.add_argument("-o", "--outdir", required = True, help = "Path to the output directory")

	args = parser.parse_args()

	kmersize = args.kmer
	level = args.level
	matrix = np.load(args.matrix)
	feat = list(np.load(args.host))
	thr = float(args.threshold)
	path = args.path
	outdir = args.outdir
	
	if "unrecognized" in feat:
		feat.remove("unrecognized")

	k = 5
	
	order = np.load("%s/order_of_samples.npy" % outdir)

	output = open("%s/results.txt" % outdir, "w")

	output2 = open("%s/probabilities.tsv" % outdir, "w")

	all_prob = []
	
	##write headers into the output2
	
	output2.write("plasmid\t")
	
	for f in feat:
		output2.write(f)
		output2.write("\t")
	output2.write("\n")

	for i in range(k):

		print("Testing round %i" % i)

		filename = "%s/%smer_%s_model_%i_dif_parameters_ete2.pkl" % (path, kmersize, level, i)

		loaded_model = joblib.load(filename)

		results = loaded_model.predict_proba(matrix)

		all_prob.append(results)

	##Take the average of the probabilities
	all_prob_aver = np.average(all_prob, axis = 0)

	for line in range(len(all_prob_aver)):
	
		probs = all_prob_aver[line]

		output.write("Predicted plasmid host range for %s is:\t" % order[line])
		output2.write("%s\t" % order[line])
		
		for a in range(len(probs)):
			output2.write("%s\t" % str(round(probs[a],3)))
			if probs[a] >= thr:
				output.write(str(feat[a]) + "\t")
	
		output.write("\n")
		output2.write("\n")

	output2.close()
	output.close()
