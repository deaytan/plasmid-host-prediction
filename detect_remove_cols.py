import numpy as np

def remove(out, feat):

	##this script  aims to remove the unrecognized features from the matrices.

	##read data

	##find unrecognized feature indexes

	ind_unrec = list(feat).index("unrecognized")

	deletem = [i for i,x in enumerate(out[:,ind_unrec]) if x == 1]

	return ind_unrec, deletem





