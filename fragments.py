import numpy as np
import collections
import random
import os
np.random.seed(42)
	
def prep_matrix(path, ids):
	##This function aims to prepare a matrix

	os.system("cat %s/*.kms | cut -f1 |sort -u > %s/all_k_mers.txt" % (path, path))

	os.system("ls %s/*.kms | rev | cut -d/ -f1 | rev> %s/all_kms.txt" % (path, path))

	kmers = np.loadtxt("%s/%s" % (path, "all_k_mers.txt"), dtype = "string", delimiter = "\t")

	ins = np.loadtxt("%s/%s" % (path, "all_kms.txt"), dtype = "string", delimiter = "\t")

	random.shuffle(ins)

	mat = np.zeros((len(ins), len(kmers)), dtype=int)
	
	kmers_dict = collections.defaultdict(list)

	##prepare the order of the input files

	order = []

	for i in ins:
		idm = i.split("_")[0]
		positions = i.split("_")[1].split(".")[0]
		
		order.append(idm)
		
	##make a dict from all possible kmers

	for i in range(len(kmers)):
		kmers_dict[kmers[i]].append(i)
	

	used_fragments = []
	used_ind = []


	for i in range(len(ins)):
		kms = np.loadtxt("%s/%s" % (path, ins[i]), dtype = "string", delimiter = "\t")

		if len(kms) > 1 and order[i] in ids:
			used_fragments.append(order[i])
			used_ind.append(i)
			for k in kms:
				mat[i, kmers_dict[k[0]]] = k[1]


	mat = mat[used_ind]

	np.save("fragment_matrix", mat)

	np.save("fragment_matrix_order", used_fragments)

	return mat, used_fragments


def matrix_fragment_output(used_fragments, ids, outputs):

	new_output = []
	new_id = []


	for i in used_fragments:

		ind = list(ids).index(i)
		new_output.append(outputs[ind])
		new_id.append(ids[ind])

	return 	np.array(new_output), new_id


def fragment_cv(new_id, k):

	uniq_elements = list(set(new_id))

	np.random.shuffle(uniq_elements)

	fold = int(len(uniq_elements)/k)
	
	s = 0
	
	cvs  = []

	for i in range(k):

		cv  = []

		if i != k-1:

			elements = uniq_elements[s:s+fold]

		else:

			elements = uniq_elements[s:]

		for e in elements:

			ind =  [y for y, x in enumerate(new_id) if x == e]						
			
			for n in ind:

				cv.append(n)

		s = s + fold

		cvs.append(cv)
		
		
	return cvs
	
def fragment_def_cv(cv_ids, val_ids, ids, new_id):

		cv_new = []
		validation_new = []


		for c in cv_ids:
			
			temp = []

			for e in c:

				element = ids[e]

				ind = [y for y,x in enumerate(new_id) if x == element]

				for n in ind:
											
					temp.append(n)

			cv_new.append(temp)


		for i in val_ids:
			
			item = ids[i]	

			ind = [a for a,b in enumerate(new_id) if b == item]
				
			for n in ind:
				
				validation_new.append(n)

		return cv_new, validation_new		


def fragment_cv_clustered(clustered_cv, new_id):

	cvs = []

	for i in clustered_cv:

		new_cv = []

		for element in i:

			ind = [y for y,x in enumerate(new_id) if x == element]
		
			for n in ind:
			
				new_cv.append(n)

					
		cvs.append(new_cv)	
	
	return cvs

	

	

	




