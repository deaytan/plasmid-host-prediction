def counts(path):

	##This script counts the number of occurences of each element in the metadata

	md_open = open(path)

	md = md_open.readlines()

	ids = []
	orgs = []

	for i in md:
		i = i.split("\t")
		ids.append(i[0])
		species = i[1].replace('"', "")
		species = species.replace("[", "").replace("]", "")
		#species = species.split(" sp.")[0]
		species = species.split(" strain")[0]
		species = species.split(" subsp.")[0]
		#species = species.replace("Candidatus ", "")
		if len(species.split()) > 2:
			if  " sp." not in species and "Candidatus" not in species:
				species = species.split()[0] + " " + species.split()[1]
			elif "Candidatus" in species:
				species = species.split()[0] + " " + species.split()[1] + " " + species.split()[2]
			else:
				species = species

		orgs.append(species)

	uniq = list(set(list(orgs)))
	

	res = []

	for i in uniq:
		if orgs.count(i) >= 5:
			if "uncultured bacterium" not in i.lower() and "bacterium symbiont" not in i.lower():
				res.append([i, orgs.count(i)])


		#else:
			#print(i)

	#print(len(res))

	return res
