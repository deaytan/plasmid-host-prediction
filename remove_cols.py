import numpy as np
import argparse 

if(__name__ == "__main__"):

	parser = argparse.ArgumentParser()

	parser.add_argument("-m", "--matrix", required= True, help = "Input matrix")
	parser.add_argument("-i", "--ids", required= True, help = "Input IDs")
	parser.add_argument("-t", "--host", required= True, help = "Host metadata")
	parser.add_argument("-f", "--feat", required= True, help = "Host names")

	args = parser.parse_args()

	def remove(matrix, id_file, out_file, features):

		##this script  aims to remove the unrecognized features from the matrices.

		##read data

		feat = np.load(features)

		out = np.load(out_file)

		mat = np.load(matrix)

		ids = np.load(id_file)

		##find unrecognized feature indexes

		ind_unrec = list(feat).index("unrecognized")

		deletem = [i for i,x in enumerate(out[:,ind_unrec]) if x == 1]

		##remove corresponding rows and cols from the matrices

		mat_new = np.delete(mat, deletem, axis = 0)

		out_new = np.delete(out, deletem, axis = 0)

		out_new = np.delete(out_new, ind_unrec, axis = 1)

		feat_new = np.delete(feat, ind_unrec)

		ids_new = np.delete(ids, deletem)

		##save the new data 

		np.save("%s_removed" % features.replace(".npy", ""), feat_new)
		np.save("%s_removed" % id_file.replace(".npy", ""), ids_new)
		np.save("%s_removed" % out_file.replace(".npy", ""), out_new)
		np.save("%s_removed" % matrix.replace(".npy", ""), mat_new)


	remove(args.matrix, args.ids, args.host, args.feat)


