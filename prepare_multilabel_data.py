from find_freq_org2 import counts
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
import collections	
import numpy as np

def prepare(path):

	all_open = open(path)
	all_read = all_open.readlines()
	
	res = counts(path)
	
	out = []
	ids = []	

	for each in res:
		for line in all_read:
			line = line.replace("[", "").replace("]", "")
			if each[0].lower() in line.lower():
				idm = line.split()[0].replace("'", "").replace('"', '')
				ids.append(idm)
				out.append(each[0])

	return ids,out

def one_hot(res):

	ids = res[0]
	out = res[1]

	feats = collections.defaultdict(list)

	# integer encode
	label_encoder = LabelEncoder()
	integer_encoded = label_encoder.fit_transform(out)

	for i in range(len(out)):
		if integer_encoded[i] not in feats:
			feats[integer_encoded[i]].append(out[i])
	
	# binary encode
	onehot_encoder = OneHotEncoder(sparse=False)
	integer_encoded = integer_encoded.reshape(len(integer_encoded), 1)
	onehot_encoded = onehot_encoder.fit_transform(integer_encoded)

	return ids, feats, onehot_encoded

def multiclass(res):
	
	ids = res[0]
	feats = res[1]
	onehot_encoded = res[2]

	deleted = []

	uniq_ids = list(set(list(ids)))

	for i in uniq_ids:
		indices = [u for u, x in enumerate(list(ids)) if x == i]

		all_ind = []
		if len(indices) > 1:
			for n in indices:
				ind = list(onehot_encoded[n]).index(1)
				all_ind.append(ind)

			min_indices = sorted(indices)[0]
			
			for m in sorted(indices)[1:]:
				deleted.append(m)
		
			for a in all_ind:
				onehot_encoded[min_indices, a] = 1


	new_mat = np.delete(onehot_encoded, deleted, axis=0)
	new_ids = np.delete(ids, deleted, axis = 0)
	new_feats = np.delete(feats, deleted, axis = 0)

	return new_ids, new_feats, new_mat
	







				
		

















