import os
import numpy as np
import argparse
import random
import sys

if(__name__ == "__main__"):

	##Ask for the inputs

	parser = argparse.ArgumentParser()

	parser.add_argument("-inp", "--inpath", required = True, help = "The path for the plasmid directory.")
	parser.add_argument("-i", "--inputlist", required = True, help = "Input list including the input plasmids")
	parser.add_argument("-l", "--level", required= True, help = "Taxonomy level")
	parser.add_argument("-t", "--threshold", required= True, help = "Class probability threshold") 
	parser.add_argument("-s", "--fragsize", required = True, help = "Fragment size; select either 500, 1000 or 1500")
	parser.add_argument("-n", "--sampling", required = True, help = "Number of times each plasmid will be sampled")
	parser.add_argument("-p", "--path", required = True, help = "Path to the machine learning models")
	parser.add_argument("-o", "--outdir", required=True, help = "Path to the output directory")

	args = parser.parse_args()

	inp = args.inpath
	inlist = np.loadtxt(args.inputlist, dtype = "string")
	level = args.level
	thr = float(args.threshold)
	frag_size = int(args.fragsize)
	sampling = int(args.sampling)
	path = args.path
	outdir = args.outdir

	rd, fn = os.path.split(os.path.abspath(__file__))

	k = 5

	##make an output directory
	os.system("mkdir %s" % outdir)

	kmers = []

	if inlist.ndim == 1:
		pass
	else:
		inlist = [inlist]

	for each in inlist:
	
		each = str(each)

		##make sure about the size:
		inpt = open("%s/%s" % (inp, each), "r")
		inpt_lines  = inpt.readlines()
	
		if ">" in inpt_lines[0]:
			inpt_merged = "".join(inpt_lines[1:]).replace("\n", "")
		else:
			inpt_merged = "".join(inpt_lines[0:]).replace("\n", "")


		if len(inpt_merged) >= frag_size:
	
 			for s in range(sampling):

				start  = random.randint(0, len(inpt_merged) - frag_size)

				fragment = inpt_merged[start:start+frag_size]

				outp = open("%s/temp.txt" % outdir, "w")

				outp.write(">tempfile\n")

				for i in fragment: 
					outp.write(i)

				outp.close()	

				##Run KMC
				os.system("kmc -t1 -k8 -fm -ci1 -cs1677215 %s/temp.txt %s/kmer.res %s/out" % (outdir, outdir, outdir))
				os.system("kmc_dump %s/kmer.res %s/%s_%s.kms" % (outdir, outdir, each.replace(".fna", "").replace(".fasta", ""), str(s)))

				kmers.append("%s_%s.kms" % (each.replace(".fna","").replace(".fasta",""), str(s)))

				##clean up
				os.system("rm -r %s/kmer.res.kmc_pre" % outdir)
				os.system("rm -r %s/kmer.res.kmc_suf" % outdir)
				os.system("rm -r %s/temp.txt" % outdir)


	##Make sure that the saved model and the new matrix has the same matrix order
	order = np.loadtxt("%s/required_input_data/order_of_8mers.txt" % rd, dtype = "string", delimiter = "\t")

	##Generate the input matrix
	mat = np.zeros((len(kmers), len(order)))
	samples =  []

	for e in range(len(kmers)):
		
		each = kmers[e]

		if each in os.listdir(outdir):

			counts = np.loadtxt("%s/%s" % (outdir, each), dtype = "string", delimiter = "\t")

			if len(counts) != 0:
				
				samples.append(each.replace(".kms", ""))

				for i in range(len(order)):

					if order[i] in list(counts[:,0]):
						ind = list(counts[:,0]).index(order[i])

						mat[e,i] = int(counts[ind,1])

	##delete empty rows
	empty = []
	for row in range(len(mat)):
		if sum(mat[row]) == 0:
			empty.append(row)
			
	mat = np.delete(mat, empty, 0)

	##save the matrix
	np.save("%s/mat" % outdir, mat)
	np.save("%s/order_of_samples" % outdir, samples)


	##Run the saved models
	os.system("python2 %s/run_saved_model_fragments.py -k 8 -l %s -m %s/mat.npy -t %s -s %s/required_input_data/plasmid_model_8mer_features_%s_ete2.npy -f %s -o %s -p %s" % (rd, level, outdir, thr, rd, level, frag_size, outdir, path))

	##cleanup
	os.system("rm -r %s/mat.npy" % outdir)

	##Show the results
	os.system("cat %s/results.txt" % outdir)
