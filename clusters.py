import collections
import numpy as np
import math
import itertools

np.random.seed(42)

def clusters(stdout):
	
	dictm = collections.defaultdict(list)

	out_o = open(stdout, "r")

	out = out_o.readlines()

	for i in out: 

		if "." in i:
			
			i = i.split("\t")

			idm = i[0]
			cls = int(i[1])

			dictm[cls].append(idm)

	out_o.close()

	return dictm


def clustered_cv(dictm, k):

	groups = dictm.keys()

	np.random.shuffle(groups)

	rang = len(groups)/k + 1

	cvs = []

	s = 0
	
	for i in range(k):
		
		cv = []
	
		for n in groups[s:s+rang]:
		
			cv.append(dictm[n])

			merged = list(itertools.chain.from_iterable(cv))

		cvs.append(list(set(merged)))

		s = s + rang

	return cvs


def clustered_ind(cvs, ids):

	new_cvs  = []

	for cv in cvs:
		temp = []
		for c in cv:
			if c in ids:
				ind = list(ids).index(c)
				temp.append(ind)
		new_cvs.append(temp)

	return new_cvs


		



