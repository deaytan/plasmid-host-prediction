import os
import numpy as np

def find_duplicates(path):

	files = os.listdir(path)

	results = []

	counts = []

	names = []

	for i in files:
		if ".kms" in i:
			temp = []
			f = np.loadtxt("%s/%s" % (path, i), dtype = "string", delimiter = "\t")

			if len(f) > 0:

				for c in f[:,1]:
					temp.append(int(c))


				if sum(temp) not in counts:
					counts.append(sum(temp))
					names.append(i)
				else:
					ind = counts.index(sum(temp))
				
					alt = np.loadtxt("%s/%s" % (path,names[ind]), dtype = "string", delimiter = "\t")

					if list(f[:,1]) == list(alt[:,1]):
						results.append(("identical", i, names[ind]))	


	return results
		


